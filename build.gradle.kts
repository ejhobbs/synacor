import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.72"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven { setUrl("https://oss.sonatype.org/content/repositories/snapshots") }
}

dependencies {
    val junit = "5.1.1"
    val kotest = "4.1.0.291-SNAPSHOT"

    implementation(kotlin("stdlib-jdk8"))

    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junit")
    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotest")
}

tasks.withType<Test> { useJUnitPlatform() }

tasks.withType(KotlinCompile::class) {
    kotlinOptions.jvmTarget = "1.8"
}