package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeCall : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(CALL.code, 0, HALT.code)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Jumps to instruction in a`() {
        val prog = listOf(CALL.code, 3, HALT.code, NOOP.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given operand register, jump to value in register`() {
        val prog = listOf(CALL.code, 0.reg(), HALT.code, NOOP.code)
        val result = prog.execute(registers = listOf(3)).execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Writes next instruction address to stack`() {
        val prog = listOf(CALL.code, 3, HALT.code, NOOP.code)
        val result = prog.execute()
        result.stack.peek() shouldBe 2
        result.stack.size shouldBe 1
    }

    @Test
    fun `Next instruction accounts for current pc`() {
        val prog = listOf(NOOP.code, CALL.code, 3, HALT.code, NOOP.code)
        val result = prog.execute().execute()
        result.stack.peek() shouldBe 3
        result.stack.size shouldBe 1
    }
}