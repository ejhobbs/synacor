package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeAnd : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(AND.code, 0, 0, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(AND.code, 0, 0, 0, NOOP.code, HALT.code)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Stores result in first operand`() {
        val prog = listOf(AND.code, 4, 10, 50, 0)
        val result = prog.execute()
        result.memory[4] shouldBe (10 maskAnd 50)
    }

    @Test
    fun `Given second operand register, read value`() {
        val prog = listOf(AND.code, 4, 0.reg(), 10, 0)
        val result = prog.execute(registers = listOf(99))
        result.memory[4] shouldBe (10 maskAnd 99)
    }

    @Test
    fun `Given third operand register, read value`() {
        val prog = listOf(AND.code, 4, 288, 1.reg(), 0)
        val result = prog.execute(registers = listOf(0, 23888))
        result.memory[4] shouldBe (288 maskAnd 23888)
    }

    @Test
    fun `Given first operand register, write there`() {
        val prog = listOf(AND.code, 2.reg(), 886234, 888888)
        val result = prog.execute(registers = listOf(34, 87, 188))
        result.registers[2] shouldBe (886234 maskAnd 888888)
    }

    private infix fun Int.maskAnd(i: Int) = (this and i) and 0x7FFF
}