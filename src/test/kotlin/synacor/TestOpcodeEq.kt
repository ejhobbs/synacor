package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeEq : MachineTest() {
    @Test
    fun `Returns status of continue`() {
        val prog = listOf(EQ.code, 4, 5, 5, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(EQ.code, 2, 5, 5, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given 2nd & 3rd operand equal set memory at 1st to 1`() {
        val prog = listOf(EQ.code, 4, 5, 5, 0)
        val result = prog.execute()
        result.memory[4] shouldBe 1
    }

    @Test
    fun `Given 2nd & 3rd operand not equal set memory at 1st to 0`() {
        val prog = listOf(EQ.code, 4, 33, 81, 1)
        val result = prog.execute()
        result.memory[4] shouldBe 0
    }

    @Test
    fun `Given first operand register, write to register`() {
        val prog = listOf(EQ.code, 0.reg(), 33, 81, 0)
        val result = prog.execute(registers = listOf(0, 0, 0, 0))
        result.registers[0] shouldBe 0
    }

    @Test
    fun `Given second operand register, read value from register`() {
        val prog = listOf(EQ.code, 4, 2.reg(), 3, 0)
        val result = prog.execute(registers = listOf(0, 0, 3, 0))
        result.memory[4] shouldBe 1
    }

    @Test
    fun `Given third operand register, read value from register`() {
        val prog = listOf(EQ.code, 4, 3, 2.reg(), 0)
        val result = prog.execute(registers = listOf(0, 0, 3, 0))
        result.memory[4] shouldBe 1
    }
}
