package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeJmp : MachineTest() {
    @Test
    fun `Advances program counter to expected value`() {
        val prog = listOf(JMP.code, 3, HALT.code, NOOP.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Use value from register if required`() {
        val prog = listOf(JMP.code, 2.reg(), HALT.code, NOOP.code)
        val registers = listOf(0, 0, 3, 0, 0)
        val result = prog.execute(registers = registers).execute()
        result.status shouldBe CONTINUE
    }
}