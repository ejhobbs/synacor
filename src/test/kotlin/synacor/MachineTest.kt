package synacor

import java.util.*

abstract class MachineTest {
    protected class Result(
        private val machine: Machine,
        val memory: IMemory,
        val registers: IMemory,
        val stack: Stack<Int>,
        initialInput: Char?
    ) {
        val status: StatusCode
            get() = this.machine.status

        val output: Char?
            get() = this.machine.output

        init {
            this.execute(initialInput)
        }

        fun execute(input: Char? = null): Result {
            machine.input = input
            machine.execute()
            return this
        }
    }

    protected fun List<Int>.execute(
        input: Char? = null,
        registers: List<Int> = emptyList(),
        stack: Stack<Int> = Stack()
    ): Result {
        val mem = ListMemory(this.toMutableList())
        val reg = ListRegister(registers.toMutableList())
        return Result(Machine(mem, reg, stack), mem, reg, stack, input)
    }

    private inner class ListRegister(private val memory: MutableList<Int>) : IMemory {
        override fun get(address: Int): Int {
            assert(address in 0..7)
            return memory[address]
        }

        override fun set(address: Int, value: Int) {
            assert(address in 0..7)
            assert(value < 32768)
            memory[address] = value
        }
    }

    private inner class ListMemory(private val memory: MutableList<Int>) : IMemory {
        private val maxAddress = 32767
        override fun get(address: Int): Int {
            assert(address in 0..maxAddress)
            return memory[address]
        }

        override fun set(address: Int, value: Int) {
            assert(address in 0..maxAddress)
            assert(value < 32768)
            memory[address] = value
        }
    }

    protected fun Int.reg() = this + 32768
    protected fun Int.stack() = Stack<Int>().also { it.push(this) }
}