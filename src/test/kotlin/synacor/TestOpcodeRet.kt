package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE
import synacor.StatusCode.END

class TestOpcodeRet : MachineTest() {
    @Test
    fun `Given stack empty set status END`() {
        val prog = listOf(RET.code)
        val result = prog.execute()
        result.status shouldBe END
    }

    @Test
    fun `Given stack not empty set status continue`() {
        val prog = listOf(RET.code)
        val result = prog.execute(stack = 0.stack())
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given stack not empty, move to next instruction`() {
        val prog = listOf(RET.code, HALT.code, NOOP.code)
        val result = prog.execute(stack = 2.stack()).execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given stack not empty, and point to register, move to instruction in register`() {
        val prog = listOf(RET.code, HALT.code, NOOP.code)
        val result = prog.execute(registers = listOf(1, 1, 2), stack = 2.reg().stack()).execute()
        result.status shouldBe CONTINUE
    }
}
