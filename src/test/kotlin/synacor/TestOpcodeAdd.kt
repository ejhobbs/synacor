package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeAdd : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(ADD.code, 0, 0, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(ADD.code, 0, 0, 0, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given first operand address, place result in address`() {
        val prog = listOf(ADD.code, 4, 3, 2, 10)
        val result = prog.execute()
        result.memory[4] shouldBe 3 + 2
    }

    @Test
    fun `Given first operand register, place result in register`() {
        val prog = listOf(ADD.code, 3.reg(), 5, 88)
        val reg = listOf(0, 0, 0, 0)
        val result = prog.execute(registers = reg)
        result.registers[3] shouldBe 5 + 88
    }

    @Test
    fun `Given second operand register, read value from register then add`() {
        val prog = listOf(ADD.code, 4, 3.reg(), 5, 0)
        val reg = listOf(0, 0, 0, 87)
        val result = prog.execute(registers = reg)
        result.memory[4] shouldBe 5 + 87
    }

    @Test
    fun `Given third operand register, read value from register then add`() {
        val prog = listOf(ADD.code, 4, 66, 2.reg(), 0)
        val reg = listOf(0, 0, 99)
        val result = prog.execute(registers = reg)
        result.memory[4] shouldBe 99 + 66
    }

    @Test
    fun `Given result gt 32768, write modulo`() {
        val prog = listOf(ADD.code, 4, 32758, 23987, 0)
        val reg = listOf(0, 0, 99)
        val result = prog.execute(registers = reg)
        result.memory[4] shouldBe (32758 + 23987) % 32768
    }
}