package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.StatusCode.CONTINUE
import synacor.StatusCode.END

class TestOpcodeNoop : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(Instruction.NOOP.code)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(Instruction.NOOP.code, Instruction.HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe END
    }
}