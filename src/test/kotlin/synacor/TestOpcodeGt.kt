package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeGt : MachineTest() {
    @Test
    fun `advances to next instruction`() {
        val prog = listOf(GT.code, 0, 0, 0, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Sets status to continue`() {
        val prog = listOf(GT.code, 0, 0, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given 2nd gt 3rd operand, set address at 1st to 1`() {
        val prog = listOf(GT.code, 4, 50, 25, 0)
        val result = prog.execute()
        result.memory[4] shouldBe 1
    }

    @Test
    fun `Given 2nd lt 3rd operand, set address at 1st to 0`() {
        val prog = listOf(GT.code, 4, 5, 25, 1)
        val result = prog.execute()
        result.memory[4] shouldBe 0
    }

    @Test
    fun `Given 2nd operand register, read from address`() {
        val prog = listOf(GT.code, 4, 0.reg(), 25, 0)
        val result = prog.execute(registers = listOf(99))
        result.memory[4] shouldBe 1
    }

    @Test
    fun `Given 3rd operand register, read from address`() {
        val prog = listOf(GT.code, 4, 25, 0.reg(), 1)
        val result = prog.execute(registers = listOf(99))
        result.memory[4] shouldBe 0
    }

    @Test
    fun `Given 1st operand register, write to register`() {
        val prog = listOf(GT.code, 0.reg(), 25, 99)
        val result = prog.execute(registers = listOf(98))
        result.registers[0] shouldBe 0
    }
}