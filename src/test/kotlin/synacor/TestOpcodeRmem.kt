package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeRmem : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(RMEM.code, 1, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(RMEM.code, 1, 0, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Writes value from b to a`() {
        val prog = listOf(RMEM.code, 3, 1, 0)
        val result = prog.execute()
        result.memory[3] shouldBe 3
    }

    @Test
    fun `If a register write to register`() {
        val prog = listOf(RMEM.code, 2.reg(), 3, 25)
        val result = prog.execute(registers = listOf(123, 632, 7777))
        result.registers[2] shouldBe 25
    }

    @Test
    fun `If b register read from address in register`() {
        val prog = listOf(RMEM.code, 1, 1.reg())
        val result = prog.execute(registers = listOf(123, 0, 1))
        result.memory[1] shouldBe RMEM.code
    }
}