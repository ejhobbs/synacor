package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeSet : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(SET.code, 0.reg(), 0)
        val result = prog.execute(registers = listOf(0, 0, 0, 0))
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(SET.code, 0.reg(), 0, NOOP.code, HALT.code)
        val result = prog.execute(registers = listOf(0, 0, 0)).execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given second operand number literal, set register to number`() {
        val prog = listOf(SET.code, 3.reg(), 20)
        val reg = listOf(1, 1, 1, 1, 1, 1, 1, 1)
        val result = prog.execute(registers = reg)
        result.registers[3] shouldBe 20
    }

    @Test
    fun `Given second operand register, set register to value of operand`() {
        val prog = listOf(SET.code, 2.reg(), 3.reg())
        val reg = listOf(0, 0, 0, 20, 0, 0, 0, 0)
        val result = prog.execute(registers = reg)
        result.registers[2] shouldBe 20
    }
}