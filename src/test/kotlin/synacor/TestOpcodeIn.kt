package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.HALT
import synacor.Instruction.IN
import synacor.StatusCode.*

class TestOpcodeIn : MachineTest() {
    @Test
    fun `Given input null, return INPUT`() {
        val program = listOf(IN.code)
        val result = program.execute(null)
        result.status shouldBe INPUT
    }

    @Test
    fun `Given input null, do not continue without input`() {
        val program = listOf(IN.code)
        val result = program.execute(null).execute(null)
        result.status shouldBe INPUT
    }

    @Test
    fun `Given input not null, return CONTINUE`() {
        val program = listOf(IN.code, 0)
        val result = program.execute('a')
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given input not null, write value to expected address`() {
        val prog = listOf(IN.code, 0)
        val result = prog.execute('f')
        result.memory[0].toChar() shouldBe 'f'
    }

    @Test
    fun `Given input not null, write value to register if address gt max`() {
        val prog = listOf(IN.code, 1.reg())
        val registers = listOf('b'.toInt(), 'a'.toInt())
        val result = prog.execute('f', registers)
        result.registers[1].toChar() shouldBe 'f'
    }

    @Test
    fun `Given input not null, advance to next instruction`() {
        val prog = listOf(IN.code, 0, HALT.code)
        val result = prog.execute('f').execute()
        result.status shouldBe END
    }
}