package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeNot : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(NOT.code, 3, 10, 123)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(NOT.code, 1, 10, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Writes inverse of 2nd operand to 1st`() {
        val prog = listOf(NOT.code, 1, 10)
        val result = prog.execute()
        result.memory[1] shouldBe 10.not()
    }

    @Test
    fun `Given 2nd operand register read from address`() {
        val prog = listOf(NOT.code, 1, 3.reg())
        val result = prog.execute(registers = listOf(0, 0, 0, 789))
        result.memory[1] shouldBe 789.not()
    }

    @Test
    fun `Given 1st operand register write to there`() {
        val prog = listOf(NOT.code, 3.reg(), 234)
        val result = prog.execute(registers = listOf(0, 0, 0, 789))
        result.registers[3] shouldBe 234.not()
    }

    @Test
    fun `Given result gt max, do mod`() {
        val prog = listOf(NOT.code, 0, 0)
        val result = prog.execute()
        result.memory[0] shouldBe 0.not()
    }

    private fun Int.not() = this.inv() and 0x7FFF
}