package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeOr : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(OR.code, 0, 0, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(OR.code, 0, 0, 0, NOOP.code, HALT.code)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Stores result in first operand`() {
        val prog = listOf(OR.code, 4, 10, 50, 0)
        val result = prog.execute()
        result.memory[4] shouldBe (10 maskOr 50)
    }

    @Test
    fun `Given second operand register, read value`() {
        val prog = listOf(OR.code, 4, 0.reg(), 10, 0)
        val result = prog.execute(registers = listOf(99))
        result.memory[4] shouldBe (10 maskOr 99)
    }

    @Test
    fun `Given third operand register, read value`() {
        val prog = listOf(OR.code, 4, 288, 1.reg(), 0)
        val result = prog.execute(registers = listOf(0, 23888))
        result.memory[4] shouldBe (288 maskOr 23888)
    }

    @Test
    fun `Given first operand register, write there`() {
        val prog = listOf(OR.code, 2.reg(), 886234, 888888)
        val result = prog.execute(registers = listOf(34, 87, 188))
        result.registers[2] shouldBe (886234 maskOr 888888)
    }

    private infix fun Int.maskOr(i: Int) = (this or i) and 0x7FFF
}