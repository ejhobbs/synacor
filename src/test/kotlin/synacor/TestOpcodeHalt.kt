package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.StatusCode.END

class TestOpcodeHalt : MachineTest() {
    @Test
    fun `Set status END`() {
        val program = listOf(Instruction.HALT.code)
        val result = program.execute()
        result.status shouldBe END
    }
}