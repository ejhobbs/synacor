package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodePop : MachineTest() {
    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(POP.code, 0, NOOP.code, HALT.code)
        val result = prog.execute(stack = 1.stack()).execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Sets status to CONTINUE`() {
        val prog = listOf(POP.code, 0)
        val result = prog.execute(stack = 1.stack())
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Sets address at first operand to value from stack`() {
        val prog = listOf(POP.code, 0)
        val result = prog.execute(stack = 43.stack())
        result.memory[0] shouldBe 43
    }

    @Test
    fun `Given first operand register write to register`() {
        val prog = listOf(POP.code, 2.reg())
        val result = prog.execute(stack = 43.stack(), registers = listOf(77, 89, 23))
        result.registers[2] shouldBe 43
    }
}