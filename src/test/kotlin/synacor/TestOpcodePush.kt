package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodePush : MachineTest() {
    @Test
    fun `set status to continue`() {
        val prog = listOf(PUSH.code, 2)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(PUSH.code, 10, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Pushes value onto stack`() {
        val prog = listOf(PUSH.code, 40)
        val result = prog.execute()
        result.stack.peek() shouldBe 40
        result.stack.size shouldBe 1
    }

    @Test
    fun `Given operand register, push value from reg`() {
        val prog = listOf(PUSH.code, 2.reg())
        val result = prog.execute(registers = listOf(0, 0, 48))
        result.stack.peek() shouldBe 48
        result.stack.size shouldBe 1
    }

}