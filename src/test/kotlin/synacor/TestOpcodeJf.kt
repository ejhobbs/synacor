package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeJf : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(JF.code, 0, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given first operand non-zero, advance to next instruction`() {
        val prog = listOf(JF.code, 1, 4, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given first operand register and non-zero, advance to next instruction`() {
        val prog = listOf(JF.code, 2.reg(), 4, NOOP.code, HALT.code)
        val registers = listOf(0, 0, 3, 0, 0, 0, 0, 0)
        val result = prog.execute(registers = registers).execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given first operand zero, jump to instruction`() {
        val prog = listOf(JF.code, 0, 4, HALT.code, NOOP.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given first operand register and zero, jump to instruction`() {
        val prog = listOf(JF.code, 1.reg(), 4, HALT.code, NOOP.code)
        val registers = listOf(0, 0, 0, 0)
        val result = prog.execute(registers = registers).execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Given first operand zero, and second is register, jump to register value`() {
        val prog = listOf(JF.code, 0, 3.reg(), HALT.code, NOOP.code)
        val registers = listOf(0, 0, 0, 4, 0, 0, 0, 0)
        val result = prog.execute(registers = registers).execute()
        result.status shouldBe CONTINUE
    }
}