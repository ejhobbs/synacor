package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.*
import synacor.StatusCode.CONTINUE

class TestOpcodeWmem : MachineTest() {
    @Test
    fun `Sets status to continue`() {
        val prog = listOf(WMEM.code, 1, 0)
        val result = prog.execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Advances to next instruction`() {
        val prog = listOf(WMEM.code, 1, 0, NOOP.code, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe CONTINUE
    }

    @Test
    fun `Writes value from b to address at a`() {
        val prog = listOf(WMEM.code, 3, 77, 0)
        val result = prog.execute()
        result.memory[3] shouldBe 77
    }

    @Test
    fun `If a register write to address from register`() {
        val prog = listOf(WMEM.code, 2.reg(), 3)
        val result = prog.execute(registers = listOf(123, 632, 1.reg()))
        result.registers[1] shouldBe 3
    }

    @Test
    fun `If b register read from register`() {
        val prog = listOf(WMEM.code, 2, 2.reg())
        val result = prog.execute(registers = listOf(123, 632, 777))
        result.memory[2] shouldBe 777
    }
}