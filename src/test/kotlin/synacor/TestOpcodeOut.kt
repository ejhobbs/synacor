package synacor

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import synacor.Instruction.HALT
import synacor.Instruction.OUT
import synacor.StatusCode.END
import synacor.StatusCode.OUTPUT

class TestOpcodeOut : MachineTest() {
    @Test
    fun `returns OUTPUT status`() {
        val program = listOf(OUT.code, 0)
        val result = program.execute()
        result.status shouldBe OUTPUT
    }

    @Test
    fun `Sets output to expected value from memory`() {
        val program = listOf(OUT.code, 'Z'.toInt())
        val result = program.execute()
        result.output shouldBe 'Z'
    }

    @Test
    fun `Once executed, advance to next instruction`() {
        val prog = listOf(OUT.code, 0, HALT.code)
        val result = prog.execute().execute()
        result.status shouldBe END
    }
}