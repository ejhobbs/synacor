import synacor.IMemory
import java.nio.file.Files
import java.nio.file.Path

class MemoryFromBinary(filename: String) : IMemory {
    private val memory: MutableMap<Int, Int> = HashMap()

    init {
        val path = Path.of(filename)
        when {
            !Files.exists(path) -> error("File not found")
            else -> {
                val bytes = path.toFile().readBytes()
                var idx = 0
                while (idx < bytes.size / 2) {
                    val lsb = (bytes[idx * 2]).toInt()
                    val msb = (bytes[(idx * 2) + 1].toInt() shl 8)
                    val value = (msb and 0xFF00) or (lsb and 0xFF)
                    assert(value in 0..32775)
                    memory[idx] = value
                    idx += 1
                }
            }
        }
    }

    override fun get(address: Int): Int = memory[address] ?: 0

    override fun set(address: Int, value: Int) {
        memory[address] = value
    }
}