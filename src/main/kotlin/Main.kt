import synacor.Machine
import synacor.StatusCode.*
import java.util.*
import kotlin.system.exitProcess

fun main() {
    val memory = MemoryFromBinary("challenge.bin")
    val registers = Registers()
    val machine = Machine(memory, registers, Stack())
    val input = ArrayList<Char>()
    var inputIdx = 0
    while (machine.execute().status != END) {
        when (machine.status) {
            INPUT -> {
                if (input.isEmpty() || inputIdx >= input.size) {
                    input.clear()
                    input.addAll(readLine()!!.toList())
                    input.add('\n')
                    inputIdx = 0
                }
                machine.input = input[inputIdx]
                inputIdx += 1
            }
            OUTPUT -> print(machine.output)
            CONTINUE -> Unit /* don't need to do anything */
            END -> exitProcess(0)
        }
    }
}