import synacor.IMemory

class Registers : IMemory {
    private val memory = Array(8) { 0 }

    override fun get(address: Int): Int {
        return memory[address]
    }

    override fun set(address: Int, value: Int) {
        memory[address] = value
    }
}