package synacor

enum class StatusCode {
    END, INPUT, CONTINUE, OUTPUT
}

