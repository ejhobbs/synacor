package synacor

import synacor.Instruction.*
import synacor.StatusCode.*
import java.util.*

class Machine(
    private val memory: IMemory,
    private val registers: IMemory,
    private val stack: Stack<Int>
) {
    var input: Char? = null
    private var _output: Char? = null
    private var _status: StatusCode = CONTINUE
    val status: StatusCode
        get() = this._status
    val output: Char?
        get() = this._output

    private var niOffset: Int = 0
    private var pc: Int = 0
    private val instruction
        get() = Instruction.valueOf(memory[pc])
    private val a
        get() = memory[pc + 1].value
    private val b
        get() = memory[pc + 2].value
    private val c
        get() = memory[pc + 3].value

    fun execute(): Machine {
        when (instruction) {
            IN -> input()
            OUT -> out()
            EQ -> (b == c).store()
            GT -> (b > c).store()
            ADD -> (b + c).store()
            MULT -> (b * c).store()
            MOD -> (b % c).store()
            AND -> (b and c).store()
            OR -> (b or c).store()
            NOT -> b.inv().store()
            JT -> (a != 0).cjmp()
            JF -> (a == 0).cjmp()
            JMP -> a.jmp()
            CALL -> a.call()
            RMEM -> b.read.store()
            WMEM -> b.store(a)
            SET -> b.store()
            PUSH -> a.push()
            POP -> stack.pop().store()
            NOOP -> cont()
            HALT -> _status = END
            RET -> ret()
        }
        pc += this.niOffset
        this.niOffset = 0
        return this
    }

    private fun Int.call() {
        stack.push(pc + instruction.operands + 1)
        this.jmp()
    }

    private fun Int.push() {
        stack.push(this)
        cont()
    }

    private fun Int.jmp() {
        cont(this - pc)
    }

    private fun Boolean.cjmp() {
        val newOffset = if (this) b - pc else null
        cont(newOffset)
    }

    private fun ret() {
        if (stack.empty()) {
            this._status = END
        } else {
            stack.pop().value.jmp()
        }
    }

    private fun input() {
        when (input) {
            null -> {
                this._status = INPUT
                this.niOffset = 0
            }
            else -> {
                input.store()
                input = null
            }
        }
    }

    private fun cont(ni: Int? = null) {
        this._status = CONTINUE
        this.niOffset = ni ?: instruction.operands + 1
    }

    private fun out() {
        this._output = a.toChar()
        this._status = OUTPUT
        this.niOffset = instruction.operands + 1
    }

    private fun Boolean.store() = (if (this) 1 else 0).store()
    private fun Char?.store() = this!!.toInt().store()
    private fun Int.store(idx: Int = memory[pc + 1]) {
        val next = instruction.operands + 1
        when (idx) {
            in 32776..65535 -> error("Invalid address $idx read from memory!")
            in 32768..32775 -> registers[idx - 32768] = this and 0x7FFF
            else -> memory[idx] = this and 0x7FFF
        }
        cont(next)
    }

    private val Int.read
        get() = when (this) {
            in 32776..65535 -> error("Invalid value $this read from memory!")
            in 32768..32775 -> registers[this - 32768]
            else -> memory[this]
        }
    private val Int.value
        get() = when (this) {
            in 32776..65535 -> error("Invalid value $this read from memory!")
            in 32768..32775 -> registers[this - 32768]
            else -> this
        }
}

