package synacor

enum class Instruction(val code: Int, val operands: Int) {
    HALT(0, 0), SET(1, 2), PUSH(2, 1), POP(3, 1),
    EQ(4, 3), GT(5, 3), JMP(6, 1), JT(7, 2),
    JF(8, 2), ADD(9, 3), MULT(10, 3), MOD(11, 3),
    AND(12, 3), OR(13, 3), NOT(14, 2), RMEM(15, 2),
    WMEM(16, 2), CALL(17, 1), RET(18, 0), OUT(19, 1),
    IN(20, 1), NOOP(21, 0);

    companion object {
        fun valueOf(code: Int) = when (code) {
            0 -> HALT
            1 -> SET
            2 -> PUSH
            3 -> POP
            4 -> EQ
            5 -> GT
            6 -> JMP
            7 -> JT
            8 -> JF
            9 -> ADD
            10 -> MULT
            11 -> MOD
            12 -> AND
            13 -> OR
            14 -> NOT
            15 -> RMEM
            16 -> WMEM
            17 -> CALL
            18 -> RET
            19 -> OUT
            20 -> IN
            21 -> NOOP
            else -> error("Unrecognised opcode $code")
        }
    }
}