package synacor

interface IMemory {
    operator fun get(address: Int): Int
    operator fun set(address: Int, value: Int)
}